﻿using System;

namespace Ctask1
{
	class Program
	{

		static void Main(string[] args)
		{
			StartGame();
		}
		public static void StartGame()
		{
			Console.WriteLine("Are you ready to make your square?");
			string answer = Console.ReadLine();
			if (answer.ToLower() == "yes")
			{
				PrintSquare();
			}
			else if (answer.ToLower() == "no")
			{
				StartGame();
			}
			else
			{
				Console.WriteLine("Command must be either yes or no, please try again");
				StartGame();
			}
		}
		public static void PrintSquare()
		{
			int Z;
			Console.WriteLine("Enter the height of your square...");
			string heightResult = Console.ReadLine();
			while (!Int32.TryParse(heightResult, out Z))
			{
				Console.WriteLine("Not a valid number, try again.");

				heightResult = Console.ReadLine();
			}
			int X = Convert.ToInt32(heightResult);

			int U;
			Console.WriteLine("Enter the width of your square...");
			string lenghtResult = Console.ReadLine();
			while (!Int32.TryParse(lenghtResult, out U))
			{
				Console.WriteLine("Not a valid number, try again.");

				lenghtResult = Console.ReadLine();
			}
			int Y = Convert.ToInt32(lenghtResult);


			for (int i = 0; i < X; i++)
			{

				for (int j = 0; j < Y; j++)
				{

					if (i == 0 || i == (X - 1) || j == 0 || j == (Y - 1))
					{
						Console.Write('*');
					}
					else
					{

						Console.Write(' ');
					}
				}
				Console.WriteLine();
			}
		}

	}
}

