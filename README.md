# Square
A small program that allows the user to create a a square of the height and width the user chooses.


## Getting Started
Open the folder in Visual studio, and run the program.cs file there.


## Author
* **Ådne Skeie** - [adnske7](https://gitlab.com/adnske7)